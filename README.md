# DJGThemeManager

ThemeManager for use with or without Sourcery and Stencil Templates

Theme and styles are loaded form JSON file 
theme.json

{
    "name":"Cacciatori di Memorie",
    "styles" : [
        {
            "name":"default",
            "backgroundColor":"#56CCF2FF",
            "textColor":"#333333FF",
            "tintColor":"#E0E0E0FF",
            "font":{
                "name":"HelveticaNeue",
                "size":36
            }
        },
        {
            "name":"green",
            "textColor":"#FFFFFFFF",
            "backgroundColor":"#219653FF"
        },
        {
            "name":"blue",
            "backgroundColor":"#56CCF2FF"
        },
        {
            "name":"bold",
            "font":{
                "name":"HelveticaNeue-Bold",
                "size":36
            }
        }
    ]
}
