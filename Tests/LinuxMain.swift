import XCTest

import DJGThemeManagerTests

var tests = [XCTestCaseEntry]()
tests += DJGThemeManagerTests.allTests()
XCTMain(tests)
