//
//  ThemeService.swift
//  Readlet
//
//  Created by Goran Djukic on 06/05/2020.
//  Copyright © 2020 Goran Djukic. All rights reserved.
//

import Foundation

let JSON_THEME_FILE_NAME = "themes"
let JSON_FILE_EXTENTION = "json"

public class ThemeService {

   public var theme:Theme?
   
    
  
    
    private static var sharedService:ThemeService = ThemeService()
    
    private init(){
        if let filePath = Bundle.main.path(forResource: JSON_THEME_FILE_NAME, ofType: JSON_FILE_EXTENTION){
            let fileManager = FileManager.default
            if let contents = fileManager.contents(atPath: filePath)  {
                let decoder = JSONDecoder()
                do{
                    let theme  = try decoder.decode(Theme.self, from:contents)
                    self.theme = theme
                } catch {
                    print("Errore nel JSON: \(error)")
                }
            }
        }
    }
    
   public static func shared()->ThemeService {
        return Self.sharedService
    }
    
    
}
