//
//  ThemeDocument.swift
//  Readlet
//
//  Created by Goran Djukic on 06/05/2020.
// Copyright © 2020 Goran Djukic. All rights reserved.
//

import UIKit

public class ThemeDocument: UIDocument {
  public var theme = Theme()
    
    public override func load(fromContents contents: Any, ofType typeName: String?) throws {
        if let contents = contents as? Data {
            let decoder = JSONDecoder()
            //            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do{
                let theme  = try  decoder.decode(Theme.self, from:contents)
                self.theme = theme
            } catch {
                print("Errore nel JSON: \(error)")
                throw NSError(domain: "NoDataDomain", code: -1, userInfo: nil)
            }
        }
    }
    
    public override func contents(forType typeName: String) throws -> Any {
        let encoder = JSONEncoder()
        //        encoder.keyEncodingStrategy = .convertToSnakeCase
        do {
            let data  = try encoder.encode(self.theme)
            return data
        } catch {
            print("Errore nel JSON: \(error)")
            throw NSError(domain: "NoDataDomain", code: -2, userInfo: nil)
        }
    }
}
