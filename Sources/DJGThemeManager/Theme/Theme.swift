//
//  Theme.swift
//  Readlet
//
//  Created by Goran Djukic on 06/05/2020.
//  Copyright © 2020 Goran Djukic. All rights reserved.
//

import UIKit

public struct Theme:Codable {
   public var name:String?
   public var styles:[Style]?
    
   public func themeStyle(name:String)->Style? {
        let style = styles?.first(where: { item -> Bool in
            item.name == name
        })
        return style
    }
    
   public func defaultStyle()->Style {
        return themeStyle(name:"default")!
    }
}
