//
//  Font.swift
//  Readlet
//
//  Created by Goran Djukic on 06/05/2020.
//  Copyright © 2020 Goran Djukic. All rights reserved.
//

import UIKit

public struct Font:Codable {
  public  var name:String?
  public  var size: CGFloat?
}


