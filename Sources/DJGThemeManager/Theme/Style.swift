//
//  Style.swift
//  Readlet
//
//  Created by Goran Djukic on 06/05/2020.
//  Copyright © 2020 Goran Djukic. All rights reserved.
//

import UIKit

public struct Style:Codable {
    public var name:String?
    public var backgroundColor:UIColor? = .white
    public var textColor: UIColor? = .black
    public var foregroundColor:UIColor? = .black
    public var tintColor:UIColor? = .white
    public var font:UIFont?
    public var kern:Float? = 1.0
    public var lineSpacing:Float? = 1.0
    public var paragraphSpacing:Float? = 10.0
    public var link:String?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        link = try? container.decode(String.self, forKey: .link)
        kern = try? container.decode(Float.self, forKey: .kern)
        lineSpacing = try? container.decode(Float.self, forKey: .lineSpacing)
        paragraphSpacing = try? container.decode(Float.self, forKey: .paragraphSpacing)

        if let font = try? container.decode(Font.self, forKey: .font){
            self.font = UIFont(name:font.name!, size: font.size ?? 18.0)
        }
        
    
        if let backgroundColor = try? container.decode(String.self, forKey: .backgroundColor){
            self.backgroundColor = UIColor.init(hex: backgroundColor)
        }
        
        if let foregroundColor = try? container.decode(String.self, forKey: .foregroundColor){
            self.foregroundColor = UIColor.init(hex: foregroundColor)
        }
        
        if let textColor = try? container.decode(String.self, forKey: .textColor){
             self.textColor = UIColor.init(hex: textColor)
        }
        if let tintColor = try? container.decode(String.self, forKey: .tintColor){
            self.tintColor = UIColor.init(hex: tintColor)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        
    }
    
    public enum CodingKeys:String, CodingKey, CaseIterable {
        case name
        case backgroundColor
        case foregroundColor
        case link
        case kern
        case lineSpacing
        case paragraphSpacing
        case textColor
        case tintColor
        case font
    }
}
